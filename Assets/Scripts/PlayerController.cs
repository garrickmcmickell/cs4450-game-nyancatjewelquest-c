﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    public KeyCode moveLeft, moveRight;

	public float speedX = 0;
    public bool linearMovement = true;
	public int score = 0;

    public Camera mainCam;
	public Text scoreText;

    private Rigidbody2D rbody;

    void OnTriggerEnter2D(Collider2D colInfo)
    {
		if (colInfo.GetComponent<Collider2D> ().tag == "Enemy") {
			score -= 10;             
		} else if (colInfo.GetComponent<Collider2D> ().tag == "Pickup") {
			score += 5;
		} else {
			SceneManager.LoadScene ("Menu");
		}

		Destroy (colInfo.gameObject);

		if (score < 0) {
			SceneManager.LoadScene ("Menu");
		} else {
			scoreText.text = score.ToString();
		}
    }
   
	// Initialization function
    void Start()
    {
        // store the rigid body in an attribute for easier access.
        rbody = GetComponent<Rigidbody2D>();
       

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(moveLeft))
        {
           
            if (linearMovement)
                rbody.velocity = new Vector2(-speedX, 0f);
            //else
                //rbody.AddForce(new Vector2(-speedX, 0f));
        }
        else if (Input.GetKey(moveRight))
        {
            if (linearMovement)
                rbody.velocity = new Vector2(speedX, 0f);
          //  else
              //  rbody.AddForce(new Vector2(speedX, 0f));
        }
        else
        {
            // no input, reset the speed
            rbody.velocity = new Vector2(0f, 0f);
           
        }
        AdjustPosition();
    }

    // function to make sure the player doesn't go off the screen
    void AdjustPosition()
    {
        Vector3 screenPos = mainCam.WorldToScreenPoint(transform.position);
        Vector3 topScreen = mainCam.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f));
        Vector3 bottomScreen = mainCam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));

        // vertical adjustment
        if (screenPos.x > Screen.width)
			transform.position = new Vector3(bottomScreen.x, transform.position.y, transform.position.z);
        else if (screenPos.x < 0)
			transform.position = new Vector3(topScreen.x, transform.position.y, transform.position.z);
        // student: add some code for the horizontal
    }
}