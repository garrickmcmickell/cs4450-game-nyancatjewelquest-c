﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMasterScript : MonoBehaviour {

	public GameObject enemy;
	public GameObject pickup;
	public GameObject player;
	public GameObject win;
	public Camera mainCam;
	public int spawnTimerSpeed = 60;
	public int dropSpeed = 5;

	private int spawnTimer;

	// Use this for initialization
	void Start () {
		spawnTimer = spawnTimerSpeed;


	}
	
	// Update is called once per frame
	void Update () {
		if (player.GetComponent<PlayerController> ().score < 50) {
			if (spawnTimer <= 0) {
				Spawn ();
				spawnTimer = spawnTimerSpeed;
			} else {
				spawnTimer--;
			}
		} else {
			win.GetComponent<Animator> ().SetBool ("isWon", true);
		}
	}

	void Spawn(){
		Vector3 screenPos = mainCam.WorldToScreenPoint(transform.position);
		Vector3 topScreen = mainCam.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f));
		Vector3 bottomScreen = mainCam.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));

		int enemyOrPickup = Random.Range (0, 2);
		Vector3 spawnPosition = new Vector3 (Random.Range (bottomScreen.x, topScreen.x), topScreen.y, 0f);

		if (enemyOrPickup == 0) {
			GameObject enemySpawn = Instantiate (enemy, spawnPosition, transform.rotation);
			enemySpawn.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, -dropSpeed);
		} else {
			GameObject pickupSpawn = Instantiate (pickup, spawnPosition, transform.rotation);
			pickupSpawn.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0f, -dropSpeed);
		}
	}
}
